import { db } from "../db/db.mjs";

export const Query = {

    hello: (_, { name }) => `Hello ${name || 'World'}`,
    getTodo : (parent, args, context, info)=>{
           
        return db.Todos;
    },

    getTodoById: (parent, {id}, context, info)=>{
        console.log(id)
        const todo =  db.Todos.find((todo) => todo.id ===id)

        if(!todo){
            throw new Error ("l'id du todo n'existe pas")
        }
        return todo;
    },
    
}