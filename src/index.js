import { GraphQLServer } from 'graphql-yoga'
import {Query} from "./resolver/Query.mjs"
// ... or using `require()`
// const { GraphQLServer } = require('graphql-yoga')

const typeDefs = "schemas/schema.graphql"
 


const resolvers = {
  Query,
}

const server = new GraphQLServer({ typeDefs, resolvers })
server.start(() => console.log('Server is running on localhost:4000'))